package com.zuzik.contactsandlocation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DBHelper extends OrmLiteSqliteOpenHelper {

	private static final String DATABASE_NAME = "dbpersons.db";
	private static final int DATABASE_VERSION = 5;
	
	private PersonDAO personDAO=null;
	
	public DBHelper(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
	}
	
	@Override
	public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
		try {
			TableUtils.createTable(arg1, Person.class);
			/*
			getPersonDAO();
			
			List<Person> persons = getRandomPersons();
			for (Person z: persons) {
				personDAO.create(z);
			}
			*/
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
			int arg3) {
		try {
			TableUtils.dropTable(arg1, Person.class, true);
			onCreate(arg0);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private List<Person> getRandomPersons(){
		List<Person> persons = new ArrayList<Person>();
		Person p1 = new Person();
		p1.setName("����");
		p1.setCountry("������");
		p1.setRegion("�����������");
		p1.setCity("�������");
		p1.setStreetName("³�����");
		p1.setStreetNumber("33");
		persons.add(p1);
		Person p2 = new Person();
		p2.setName("����");
		p2.setCountry("������");
		p2.setRegion("�����������");
		p2.setCity("�������");
		p2.setStreetName("��� ����������");
		p2.setStreetNumber("3");
		persons.add(p2);
		return persons;
	}
	
	public PersonActionInterface getPersonDAO() throws SQLException{
		if(personDAO==null){
			personDAO = new PersonDAO(getConnectionSource(), Person.class);
		}
		return personDAO;
	}
	
	@Override
	public void close() {
		super.close();
		personDAO = null;
	}

	
}
