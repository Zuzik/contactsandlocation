package com.zuzik.contactsandlocation;

import java.sql.SQLException;
import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class DbListActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        HelperFactory.setHelper(this);
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	setArrayAdapter();
    }
    
    @Override
    protected void onDestroy() {
    	HelperFactory.releaseHelper();
    	super.onDestroy();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	getMenuInflater().inflate(R.menu.menu_activity_db_list, menu);
    	return true;
    }
    
    private void setArrayAdapter(){
		try {
			List<Person> persons = HelperFactory.getHelper().getPersonDAO().getAllPersons();
			ArrayAdapter<Person> arr = new ArrayAdapter<Person>(this, android.R.layout.simple_list_item_1, persons);
	    	setListAdapter(arr);
		} catch (SQLException e) {
			ViewMessage.showToast(this, R.string.msg_db_error_read_all);
		}
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
		case R.id.menu_activity_db_list_add_person:
			createPerson();
			return true;
		default:
			return false;
		}
    }
    
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
    	super.onListItemClick(l, v, position, id);
    	Person p = (Person) l.getAdapter().getItem(position);
    	startActivity(ShowPersonActivity.getInstanceIntent(this, p.getId()));
    }
    
    void createPerson(){
    	startActivity(EditPersonActivity.getInstanceIntent(this));
    }
    
}
