package com.zuzik.contactsandlocation;

import java.sql.SQLException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class EditPersonActivity extends Activity {

	public static final String KEY_PERSON_ID = "EditPersonActivity.KEY_PERSON_ID";
	private static final int EMPTY_ID = -1;
	
	public static Intent getInstanceIntent(Context context, int personId){
		Intent i = new Intent (context, EditPersonActivity.class);
		i.putExtra(KEY_PERSON_ID, personId);
		return i;
	}
	
	public static Intent getInstanceIntent(Context context){
		return getInstanceIntent(context, EMPTY_ID);
	}
	
	
	EditText textName, textCountry, textRegion, textCity,
		textStreetName, textStreetNumber;
	
	private Person person = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_person);
		
		findViews();

	}

	private void findViews() {
		textName = (EditText)findViewById(R.id.text_person_name);
		textCountry = (EditText)findViewById(R.id.text_person_country);
		textRegion = (EditText)findViewById(R.id.text_person_region);
		textCity = (EditText)findViewById(R.id.text_person_city);
		textStreetName = (EditText)findViewById(R.id.text_person_street_name);
		textStreetNumber = (EditText)findViewById(R.id.text_person_street_number);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		handleInstanceIntent();
	}
	
	private void handleInstanceIntent(){
		int person_id = getIntent().getIntExtra(KEY_PERSON_ID, -1);
		if(person_id==EMPTY_ID){
			person = new Person();
		} else {
			try {
				person = HelperFactory.getHelper().getPersonDAO().getPerson(person_id);
				showPerson(person);
			} catch (SQLException e) {
				ViewMessage.showToast(this, R.string.msg_db_error_get_person);
				this.finish();
			}
		}
	}
	
	private void showPerson(Person p){
		textName.setText(p.getName());
		/*
		if(!isTextEquals(textCountry.getText().toString(), p.getCountry()) ||
				!isTextEquals(textCity.getText().toString(), p.getCity()) ||
				!isTextEquals(textStreetName.getText().toString(), p.getCity()) ||
				!isTextEquals(textStreetNumber.getText().toString(), p.getStreetNumber()))
			*/
		textCountry.setText(p.getCountry());
		textRegion.setText(p.getRegion());
		textCity.setText(p.getCity());
		textStreetName.setText(p.getStreetName());
		textStreetNumber.setText(p.getStreetNumber());
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_activity_edit_person, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_activity_edit_person_save_person:
			if(savePerson()){
				this.finish();
			}
			return true;
		default:
			return false;
		}
	}
	
	private boolean isInputDataCorrect(){
		return isEditTextNotEmpty(textName, R.string.msg_not_entered_name) &&
			isEditTextNotEmpty(textCountry, R.string.msg_not_entered_country) &&
			//isEditTextNotEmpty(textRegion, R.string.msg_not_entered_region) &&
			isEditTextNotEmpty(textCity, R.string.msg_not_entered_city) &&
			isEditTextNotEmpty(textStreetName, R.string.msg_not_entered_street_name) &&
			isEditTextNotEmpty(textStreetNumber, R.string.msg_not_entered_street_number);
	}
	
	private boolean isEditTextNotEmpty(EditText text, int msgId){
		if(isTextEmpty(text)){
			ViewMessage.showToast(this, msgId);
			text.requestFocus();
			return false;
		} else {
			return true;
		}
	}
	
	private boolean isTextEmpty(EditText text){
		return text.getText().length()==0;
	}
	
	private boolean savePerson(){
		if(isInputDataCorrect()){
			if(isLocationChanged()){
				person.setLatitude(Person.DEFAULT_COORDINATE);
				person.setLongitude(Person.DEFAULT_COORDINATE);
			}
			person.setName(textName.getText().toString());
			person.setCountry(textCountry.getText().toString());
			person.setRegion(textRegion.getText().toString());
			person.setCity(textCity.getText().toString());
			person.setStreetName(textStreetName.getText().toString());
			person.setStreetNumber(textStreetNumber.getText().toString());
			writePersonToDB();
			return true;
		} else{
			return false;
		}
	}
	
	private boolean isLocationChanged(){
		return !isTextEquals(textCountry.getText().toString(), person.getCountry()) ||
				!isTextEquals(textRegion.getText().toString(), person.getRegion()) ||
				!isTextEquals(textCity.getText().toString(), person.getCity()) ||
				!isTextEquals(textStreetName.getText().toString(), person.getStreetName()) ||
				!isTextEquals(textStreetNumber.getText().toString(), person.getStreetNumber());
	}
	
	private boolean isTextEquals(String textOne, String textTwo){
		return textOne.equalsIgnoreCase(textTwo);
	}
	
	private void writePersonToDB(){
		try {
			HelperFactory.getHelper().getPersonDAO().createOrUpdatePerson(person);
			ViewMessage.showToast(this, R.string.msg_person_save_success);
		} catch (SQLException e) {
			ViewMessage.showToast(this, R.string.msg_db_error_save_person);
		}
	}
	
}
