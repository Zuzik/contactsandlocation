package com.zuzik.contactsandlocation;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName=Person.TABLE_NAME)
public class Person {

	public static final String TABLE_NAME = "persons";
	
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_COUNTRY = "country";
	public static final String COLUMN_REGION = "region";
	public static final String COLUMN_CITY = "city";
	public static final String COLUMN_STREET_NAME = "street_name";
	public static final String COLUMN_STREET_NUMBER = "street_number";
	public static final String COLUMN_LATITUDE = "latitude";
	public static final String COLUMN_LONGITUDE = "longitude";
	
	public static final double DEFAULT_COORDINATE = 0.0;
	
	@DatabaseField(columnName=COLUMN_ID, dataType=DataType.INTEGER, generatedId=true)
	private int mId;
	@DatabaseField(columnName=COLUMN_NAME, dataType=DataType.STRING, defaultValue="")
	private String mName;
	@DatabaseField(columnName=COLUMN_COUNTRY, dataType=DataType.STRING, defaultValue="")
	private String mCountry;
	@DatabaseField(columnName=COLUMN_REGION, dataType=DataType.STRING, defaultValue="")
	private String mRegion;
	@DatabaseField(columnName=COLUMN_CITY, dataType=DataType.STRING, defaultValue="")
	private String mCity;
	@DatabaseField(columnName=COLUMN_STREET_NAME, dataType=DataType.STRING, defaultValue="")
	private String mStreetName;
	@DatabaseField(columnName=COLUMN_STREET_NUMBER, dataType=DataType.STRING, defaultValue="")
	private String mStreetNumber;
	@DatabaseField(columnName=COLUMN_LATITUDE, dataType=DataType.DOUBLE)
	private double mLatitude=DEFAULT_COORDINATE;
	@DatabaseField(columnName=COLUMN_LONGITUDE, dataType=DataType.DOUBLE)
	private double mLongitude=DEFAULT_COORDINATE;
	
	public Person(){
		
	}

	public Integer getId() {
		return mId;
	}

	public void setId(Integer id) {
		mId = id;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public String getCountry() {
		return mCountry;
	}

	public void setCountry(String country) {
		mCountry = country;
	}

	public String getRegion() {
		return mRegion;
	}

	public void setRegion(String region) {
		mRegion = region;
	}

	public String getCity() {
		return mCity;
	}

	public void setCity(String city) {
		mCity = city;
	}

	public String getStreetName() {
		return mStreetName;
	}

	public void setStreetName(String streetName) {
		mStreetName = streetName;
	}

	public String getStreetNumber() {
		return mStreetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		mStreetNumber = streetNumber;
	}

	public double getLatitude() {
		return mLatitude;
	}

	public void setLatitude(Double latitude) {
		mLatitude = latitude;
	}

	public double getLongitude() {
		return mLongitude;
	}

	public void setLongitude(Double longitude) {
		mLongitude = longitude;
	}
	
	@Override
	public String toString() {
		return getName();
	}
	
}
