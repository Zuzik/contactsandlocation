package com.zuzik.contactsandlocation;

import java.sql.SQLException;
import java.util.List;

public interface PersonActionInterface {

	public List<Person> getAllPersons() throws SQLException;
	public Person getPerson(int id) throws SQLException;
	public void createOrUpdatePerson(Person person) throws SQLException;
	public void removePerson(int id) throws SQLException;
	public void removePerson(Person person) throws SQLException;
	
}
