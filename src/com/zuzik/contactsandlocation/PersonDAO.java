package com.zuzik.contactsandlocation;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

public class PersonDAO extends BaseDaoImpl<Person, Integer> implements PersonActionInterface {

	public PersonDAO(ConnectionSource connectionSource, Class<Person> dataClass)
			throws SQLException {
		super(connectionSource, dataClass);
	}

	@Override
	public List<Person> getAllPersons() throws SQLException {
		return this.queryForAll();
	}

	@Override
	public Person getPerson(int id) throws SQLException  {
		return this.queryForId(id);
	}

	@Override
	public void createOrUpdatePerson(Person person)  throws SQLException {
		this.createOrUpdate(person);
	}

	@Override
	public void removePerson(int id) throws SQLException  {
		this.deleteById(id);
	}

	@Override
	public void removePerson(Person person)  throws SQLException {
		this.delete(person);
	}
	
}
