package com.zuzik.contactsandlocation;

import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class ShowLocationActivity extends Activity {

	public static final String KEY_PERSON_ID = "ShowLocationActivity.KEY_PERSON_ID";
	
	public static Intent getInstanceIntent(Context context, int personId){
		Intent i = new Intent (context, ShowLocationActivity.class);
		i.putExtra(KEY_PERSON_ID, personId);
		return i;
	}
	
	private GoogleMap mMap;
	
	private int personId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_location);
		handleInstanceIntent();
		setupMapIfNeed();
	}

	private void handleInstanceIntent(){
		personId = getIntent().getIntExtra(KEY_PERSON_ID, -1);
	}
	
	private void setupMapIfNeed(){
    	if(mMap==null){
    		mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
    		if(mMap!=null){
    			ViewMessage.showToast(this, "Not null");
    			new ShowLocationTask().execute(personId);
    		} else {
    			ViewMessage.showToast(this, "Null");
    		}
    	}
    }
	
	private void showCantFindLocationMsg(){
		ViewMessage.showToast(this, R.string.msg_error_find_location);
		this.finish();
	}

	private class ShowLocationTask extends AsyncTask<Integer, Void, Person>{
    	
		private String getAddress(Person p){
			return new StringBuilder().append(p.getStreetName()).append(",")
					.append(p.getStreetNumber()).append(",")
					.append(p.getCity()).append(",")
					.append(p.getRegion()).append(",")
					.append(p.getCountry()).toString();
		}
		
		@Override
		protected Person doInBackground(Integer... params) {
			int personId = params[0];
			Person person = null;
			try{
				person = HelperFactory.getHelper().getPersonDAO().getPerson(personId);
				boolean locationExist = person.getLatitude() != Person.DEFAULT_COORDINATE
						&& person.getLongitude()!=Person.DEFAULT_COORDINATE;
				if(!locationExist){
					Geocoder gc = new Geocoder(getApplicationContext(), Locale.ENGLISH);
					List<Address> addresses = gc.getFromLocationName(getAddress(person), 1);
					if(addresses.size()>0){
						Address address = addresses.get(0);
						person.setLatitude(address.getLatitude());
						person.setLongitude(address.getLongitude());
						HelperFactory.getHelper().getPersonDAO().createOrUpdatePerson(person);
					} else {
						person = null;
					}
				}
			} catch (Exception e){
				person = null;
			}
			return person;
		}

		@Override
		protected void onPostExecute(Person result) {
			super.onPostExecute(result);
			if(result!=null){
				double lat = result.getLatitude();
				double lgt = result.getLongitude();
				String title = result.getName();
				int zoom = 10;
				LatLng pos = new LatLng(lat, lgt);
				mMap.addMarker(new MarkerOptions().position(pos).title(title));
				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, zoom));
			} else {
				showCantFindLocationMsg();
			}
		}
    	
    }
}
