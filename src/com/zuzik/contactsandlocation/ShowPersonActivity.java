package com.zuzik.contactsandlocation;

import java.sql.SQLException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ShowPersonActivity extends Activity {

	public static final String KEY_PERSON_ID = "ShowPersonActivity.KEY_PERSON_ID";
	
	public static Intent getInstanceIntent(Context context, int personId){
		Intent i = new Intent (context, ShowPersonActivity.class);
		i.putExtra(KEY_PERSON_ID, personId);
		return i;
	}
	
	TextView textName, textCountry, textRegion, textCity,
		textStreetName, textStreetNumber, textLatitude, textLongitude;
	
	private Person person = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_person);
		
		findViews();
	}
	
	private void findViews(){
		textName = (TextView)findViewById(R.id.text_person_name);
		textCountry = (TextView)findViewById(R.id.text_person_country);
		textRegion = (TextView)findViewById(R.id.text_person_region);
		textCity = (TextView)findViewById(R.id.text_person_city);
		textStreetName = (TextView)findViewById(R.id.text_person_street_name);
		textStreetNumber = (TextView)findViewById(R.id.text_person_street_number);
		textLatitude = (TextView)findViewById(R.id.text_person_latitude);
		textLongitude = (TextView)findViewById(R.id.text_person_longitude);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		handleInstanceIntent();
	}
	
	private void handleInstanceIntent(){
		int person_id = getIntent().getIntExtra(KEY_PERSON_ID, -1);
		try {
			person = HelperFactory.getHelper().getPersonDAO().getPerson(person_id);
			showPerson(person);
		} catch (SQLException e) {
			ViewMessage.showToast(this, R.string.msg_db_error_get_person);
		}
	}
	
	private void showPerson(Person p){
		textName.setText(p.getName());
		textCountry.setText(p.getCountry());
		textRegion.setText(p.getRegion());
		textCity.setText(p.getCity());
		textStreetName.setText(p.getStreetName());
		textStreetNumber.setText(p.getStreetNumber());
		
		if(p.getLatitude()!=Person.DEFAULT_COORDINATE && p.getLongitude()!=Person.DEFAULT_COORDINATE){
			textLatitude.setText(Double.toString(p.getLatitude()));
			textLongitude.setText(Double.toString(p.getLongitude()));	
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_activity_show_person, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_activity_show_person_show_map:
			startMapLocationActivity();
			return true;
		case R.id.menu_activity_show_person_edit_person:
			startEditActivity();
			return true;
		case R.id.menu_activity_show_person_remove_person:
			removePerson();
			return true;
		default:
			return false;
		}
	}
	
	private void startEditActivity(){
		startActivity(EditPersonActivity.getInstanceIntent(this, person.getId()));
	}
	
	private void removePerson(){
		try {
			HelperFactory.getHelper().getPersonDAO().removePerson(person);
			ViewMessage.showToast(this, R.string.msg_person_remove_success);
		} catch (SQLException e) {
			ViewMessage.showToast(this, R.string.msg_db_error_remove_person);
		}
		this.finish();
	}
	
	private void startMapLocationActivity(){
		startActivity(ShowLocationActivity.getInstanceIntent(this, person.getId()));
	}
	
}
