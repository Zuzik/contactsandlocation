package com.zuzik.contactsandlocation;

import android.content.Context;
import android.widget.Toast;

public class ViewMessage {

	public static void showToast(Context context, int msgId){
		Toast.makeText(context, msgId, Toast.LENGTH_SHORT).show();
	}
	
	public static void showToast(Context context, String msg){
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}
	
}
